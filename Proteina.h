//Proteina.h
#include <iostream>
#include <list>
using namespace std;
#include "Cadena.h"

#ifndef PROTEINA_H
#define PROTEINA_H

class Proteina {
	private:
		string nombre;
		string id;
		list<Cadena> cadenas;

	public:
		
		Proteina(string nombre, string id);
		
		
		string get_nombre();
		string get_id();
		
		void set_nombre(string nombre);
		void set_id(string id);
		void add_cadena(Cadena cadena);
		list<Cadena> get_cadenas();
};
#endif
