/*
 * g++ programa.cpp Proteina.cpp -o programa
 */
#include <iostream>
#include <list>
#include <stdlib.h>
#include <stdio.h>
#include "Proteina.h"
#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"

void imprimir_datos_proteinas() {
	for (Proteina p : Proteina)
	{
		cout << p.get_nombre() << endl;
	}
	
}

void ingresar_datos_proteinas() {
	
	list<Proteina> proteina;

	cout << "¿Cuantás proteinas deseas ingresar?" << endl;
	int nro_proteinas;
	cin >> nro_proteinas;
	cout << "Ingresa la información para 1 proteína" << endl;
	for (int i = 0; i < nro_proteinas; i++){
		//Este For ingresa la información de cada proteína
		cout << "Ingresa la ID de la proteína: " << endl;
		string id_proteina;
		cin >> id_proteina;
		cout << "Ingresa el nombre de la Proteína: " << endl; 
		string nombre_proteina;
		cin >> nombre_proteina;
		Proteina p = Proteina(nombre_proteina, id_proteina);
		
		
		cout << "¿Cuántas cadenas tiene la proteína?" << endl;
		int nro_cadenas;
		cin >> nro_cadenas;
		for(int j = 0; j < nro_cadenas; j++){
			//FOR que permite el ingreso de la información de cada cadena de la proteína
			cout << "Ingresa la LETRA de la cadena: " << endl;
			string letra_cadena;
			cin >> letra_cadena;
			//instancia cadena
			Cadena c = Cadena(letra_cadena);
			
		
			cout << "¿Cuántos Aminoácidos contiene la cadena?" << endl;
			int cant_aminoacidos;
			cin >> cant_aminoacidos;
			for(int k = 0; k < cant_aminoacidos; k++){
				//FOR que permite el ingreso de la info de cada aminoácido
				cout << "Ingresa el nombre de un aminoacido: " << endl;
				string nombre_aminoacido;
				cin >> nombre_aminoacido;
				cout << "Ingresa el número del aminoacido: " << endl;
				int nro_aminoacido;
				cin >> nro_aminoacido;
				Aminoacido aminoacid = Aminoacido(nombre_aminoacido, nro_aminoacido); 
				
				
				cout << "¿Cuántos átomos contiene este aminoácido?" << endl;
				int cant_atomos;
				cin >> cant_atomos;
				for(int l = 0; l < cant_atomos; l++){
					//For que permite el ingreso de la info de cada atomo
					cout << "Ingresa el nombre del átomo: " << endl;
					string nombre_atomo;
					cin >> nombre_atomo;
					cout << "Ingresa el número del átomo: " << endl;
					int nro_atomo;
					cin >> nro_atomo;
					Atomo atomo = Atomo(nombre_atomo, nro_atomo);
					
					cout << "Ingresa la coordenada X del átomo: " << endl;
					float coordenada_x;
					cin >> coordenada_x;
					cout << "Ingresa la coordenada Y del átomo: " << endl;
					float coordenada_y;
					cin >> coordenada_y;
					cout << "Ingrese la coordenada Z del átomo: " << endl;
					float coordenada_z;
					cin >> coordenada_z;

					Coordenada coordenada = Coordenada(coordenada_x, coordenada_y, coordenada_z);
					atomo.set_coordenada(coordenada);
					aminoacid.add_atomo(atomo);
				}
				c.add_aminoacido(aminoacid);
			}
			// Agrega cadena
			p.add_cadena(c);
		}
		proteina.push_back(p);
	}
	//imprimir_datos_proteinas(proteina);
}



int main(){

	int opcion;
		system("clear");
		cout << "**********MENÚ********** " << endl;
		cout << "Ingresa una de las opciones:" << endl;
		cout << "1. Ingresar Proteina " << endl;
		cout << "2. Ver Proteinas " << endl;
		cout << "3. Salir del Programa " << endl;
		
		cin  >> opcion;

		if (opcion == 1) {
		
			ingresar_datos_proteinas();
		}else if (opcion == 2) {
			
			//imprimir_datos_proteinas();
			cout << "OPCION NO DISPONIBLE POR AHORA" << endl;
			main();
		}else if (opcion == 3) {
			cout << "Gracias por usar este programa. Hasta Pronto.";
			exit(0);
		}else if (opcion != 3) {
		 cout << " Opcion Invalida. Solo se permiten tres opciones...";
		
		main();
	}
	
	return 0;
}