// Aminoacido.h
 
#include <list>
#include <iostream>
using namespace std;
#include "Atomo.h"

#ifndef AMINOACIDO_H
#define AMINOACIDO_H

class Aminoacido {
	private:
		string nombre;
		int numero;
		list<Atomo> atomos;

	public:
		
		Aminoacido (string nombre, int numero);
		
		
		string get_nombre();
		int get_numero();
		
		void set_nombre(string nombre);
		void set_numero(int numero);
		void add_atomo(Atomo atomo);
		list<Atomo> get_atomos();
};
#endif
